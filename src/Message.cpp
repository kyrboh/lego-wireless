#include "legowl/Message.hpp"

#include "legowl/Messages.hpp"
#include "legowl/util/Constant.hpp"
#include "legowl/util/TypeTraits.hpp"
#include "legowl/util/Visitor.hpp"

#include <cassert>

namespace legowl {
namespace {
template <typename Message>
using CanBeRead = decltype(Message::read(std::declval<CommonMessageHeader>(),
                                         std::declval<Span<const std::uint8_t>>()));
} // namespace

std::unique_ptr<Message> Message::read(Span<const std::uint8_t> bytes) {
  CommonMessageHeader header = CommonMessageHeader::read(bytes);
  auto payload = bytes.subspan(header.size());

  using namespace std::literals::string_literals;

  return route(header.messageType(), [&](auto messageType) -> std::unique_ptr<Message> {
    using MessageType = MessageImpl<messageType()>;

    // NOTE: although isCompleteType trait is conceptually wrong
    //       it fits perfectly this application: if message type is not implemented,
    //       it is not going to appear enywhere as complete, thus not breaking the ODR
    if constexpr (isCompleteType<MessageType> && isDetected<CanBeRead, MessageType>) {
      return MessageType::read(header, payload);
    } else {
      throw std::runtime_error("unexpected message type: "s.append(toString(messageType())));
    }
  });
}

void Message::write(Span<std::uint8_t> buffer) const
{
  auto payloadBuffer = _header.write(buffer);
  serialize(payloadBuffer);
}

void Message::serialize(Span<std::uint8_t>) const {
  throw std::invalid_argument("Serialization for this message is not implemented");
}

std::string Message::asString() const {
  std::string result = "Message{length=" + std::to_string(_header.messageLength()) +
                       ", type=" + std::string{legowl::toString(_header.messageType())} + "}";
  return result;
}

} // namespace legowl
