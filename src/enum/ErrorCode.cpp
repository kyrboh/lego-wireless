#include "legowl/enum/ErrorCode.hpp"

namespace legowl {

std::string_view toString(ErrorCode ec) {
  switch (ec) {
  case ErrorCode::ACK: return "ACK";
  case ErrorCode::MACK: return "MACK";
  case ErrorCode::BufferOverflow: return "BufferOverflow";
  case ErrorCode::Timeout: return "Timeout";
  case ErrorCode::CommandNotRecognized: return "CommandNotRecognized";
  case ErrorCode::InvalidUse: return "InvalidUse";
  case ErrorCode::Overcurrent: return "Overcurrent";
  case ErrorCode::InternalError: return "InternalError";
  default: return "ErrorCode{???}";
  }
}

} // namespace legowl
