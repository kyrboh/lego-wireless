#include "legowl/enum/IOTypeID.hpp"

namespace legowl {

std::string_view toString(IOTypeID ioType) {
  switch (ioType) {
  case IOTypeID::Motor: return "Motor";
  case IOTypeID::SystemTrainMotor: return "SystemTrainMotor";
  case IOTypeID::Button: return "Button";
  case IOTypeID::LEDLight: return "LEDLight";
  case IOTypeID::Voltage: return "Voltage";
  case IOTypeID::Current: return "Current";
  case IOTypeID::PiezoToneSound: return "PiezoToneSound";
  case IOTypeID::RGBLight: return "RGBLight";
  case IOTypeID::ExternalTiltSensor: return "ExternalTiltSensor";
  case IOTypeID::MotionSensor: return "MotionSensor";
  case IOTypeID::VisionSensor: return "VisionSensor";
  case IOTypeID::ExternalMotorWithTacho: return "ExternalMotorWithTacho";
  case IOTypeID::InternalMotorWithTacho: return "InternalMotorWithTacho";
  case IOTypeID::InternalTilt: return "InternalTilt";
  case IOTypeID::PoweredUpXLMotor: return "PoweredUpXLMotor";
  case IOTypeID::PoweredUpLMotor: return "PoweredUpLMotor";
  default: return "IOTypeID{???}";
  }
}

} // namespace legowl
