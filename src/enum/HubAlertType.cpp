#include "legowl/enum/HubAlertType.hpp"

namespace legowl {

std::string_view toString(HubAlertType alertType) {
  switch (alertType) {
  case HubAlertType::LowVoltage: return "LowVoltage";
  case HubAlertType::HighCurrent: return "HighCurrent";
  case HubAlertType::LowSignalStrength: return "LowSignalStrength";
  case HubAlertType::OverPowerCondition: return "OverPowerCondition";
  default: return "HubAlertType{???}";
  }
}

} // namespace legowl
