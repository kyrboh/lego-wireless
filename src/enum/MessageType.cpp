#include "legowl/enum/MessageType.hpp"

#include <cassert>
#include <ostream>

namespace legowl {

std::ostream &operator<<(std::ostream &os, MessageType messageType) {
  return os << toString(messageType);
}

std::string_view toString(MessageType messageType) {
  switch (messageType) {
  case MessageType::HubProperties: return "HubProperties";
  case MessageType::HubActions: return "HubActions";
  case MessageType::HubAlerts: return "HubAlerts";
  case MessageType::HubAttachedIO: return "HubAttachedIO";
  case MessageType::GenericErrorMessages: return "GenericErrorMessages";
  case MessageType::HWNetworkCommands: return "HWNetworkCommands";
  case MessageType::FWUpdateGoIntoBootMode: return "FWUpdateGoIntoBootMode";
  case MessageType::FWUpdateLockMemory: return "FWUpdateLockMemory";
  case MessageType::FWUpdateLockStatusRequest: return "FWUpdateLockStatusRequest";
  case MessageType::FWLockStatus: return "FWLockStatus";
  case MessageType::PortInformationRequest: return "PortInformationRequest";
  case MessageType::PortModeInformationRequest: return "PortModeInformationRequest";
  case MessageType::PortInputFormatSetupSingle: return "PortInputFormatSetupSingle";
  case MessageType::PortInputFormatSetupCombined: return "PortInputFormatSetupCombined";
  case MessageType::PortInformation: return "PortInformation";
  case MessageType::PortModeInformation: return "PortModeInformation";
  case MessageType::PortValueSingle: return "PortValueSingle";
  case MessageType::PortValueCombined: return "PortValueCombined";
  case MessageType::PortInputFormatSingle: return "PortInputFormatSingle";
  case MessageType::PortInputFormatCombined: return "PortInputFormatCombined";
  case MessageType::VirtualPortSetup: return "VirtualPortSetup";
  case MessageType::PortOutputCommand: return "PortOutputCommand";
  case MessageType::PortOutputCommandFeedback: return "PortOutputCommandFeedback";
  default: return "MessageType{???}";
  }
}

} // namespace legowl
