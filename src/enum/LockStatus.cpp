#include "legowl/enum/LockStatus.hpp"

namespace legowl {

std::string_view toString(LockStatus status) {
  switch (status) {
  case LockStatus::Ok: return "Ok";
  case LockStatus::NotLocked: return "NotLocked";
  default: return "LockStatus{???}";
  }
}

} // namespace legowl
