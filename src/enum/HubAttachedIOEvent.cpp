#include "legowl/enum/HubAttachedIOEvent.hpp"

namespace legowl {

std::string_view toString(HubAttachedIOEvent event) {
  switch (event) {
  case HubAttachedIOEvent::DetachedIO: return "DetachedIO";
  case HubAttachedIOEvent::AttachedIO: return "AttachedIO";
  case HubAttachedIOEvent::AttachedVirtualIO: return "AttachedVirtualIO";
  default: return "HubAttachedIOEvent{???}";
  }
}

} // namespace legowl
