#include "legowl/enum/PortClass.hpp"

namespace legowl {

PortClass toPortClass(uint8_t portId) noexcept {
  if (portId <= 49)
    return PortClass::HubConnector;

  if (portId <= 100)
    return PortClass::Internal;

  return PortClass::Reserved;
}

} // namespace legowl
