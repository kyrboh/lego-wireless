#include "legowl/enum/HubAlertOperation.hpp"

namespace legowl {

std::string_view toString(HubAlertOperation operation) {
  switch (operation) {
  case HubAlertOperation::EnableUpdates: return "EnableUpdates";
  case HubAlertOperation::DisableUpdates: return "DisableUpdates";
  case HubAlertOperation::RequestUpdates: return "RequestUpdates";
  case HubAlertOperation::Update: return "Update";
  default: return "HubAlertOperation{???}";
  }
}

} // namespace legowl
