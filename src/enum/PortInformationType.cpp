#include "legowl/enum/PortInformationType.hpp"

namespace legowl {

std::string_view toString(PortInformationType infoType)
{
  switch (infoType)
  {
  case PortInformationType::PortValue: return "PortValue";
  case PortInformationType::ModeInfo: return "ModeInfo";
  case PortInformationType::PossibleModeCombinations: return "PossibleModeCombinations";
  default: return "PortInformationType{???}";
  }
}

} // namespace legowl
