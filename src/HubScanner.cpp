#include "legowl/HubScanner.hpp"

#include "legowl/MessageVisitor.hpp"
#include "legowl/message/HubAttachedIO.hpp"
#include "legowl/model/Hub.hpp"
#include "legowl/model/Motor.hpp"
#include "legowl/util/TypeTraits.hpp"

#include <iostream>

namespace legowl {

void HubScanner::process(const Message &message) {
  visit(message, [&](const auto &msg) { handle(msg); });
}

std::unique_ptr<Message> HubScanner::poll() {
  if (_outbox.empty())
    return nullptr;

  auto message = std::move(_outbox.front());
  _outbox.pop();
  return message;
}

void HubScanner::handle(const Message &unsupportedMsg) {
  throw std::invalid_argument("cannot process message: " + unsupportedMsg.asString());
}

void HubScanner::handle(const HubAttachedIOBasicMessage &message) {
  message.visit([&](const auto &msg) { handle(msg); });
}

void HubScanner::handle(const HubAttachedIOMessage &message) {
  using namespace std::literals::string_literals;
  const IOTypeID ioType = message.ioTypeId();
  switch (ioType) {
  case IOTypeID::Motor:
  case IOTypeID::PoweredUpLMotor:
  case IOTypeID::PoweredUpXLMotor:
    _hub->connect(message.portId(), std::make_unique<Motor>(ioType));
    break;
  default:
    if (message.portClass() == PortClass::HubConnector)
      throw std::runtime_error("unsupported IO type connected: "s.append(toString(ioType)));
  }
}

void HubScanner::handle(const HubAttachedVirtualIOMessage &message) {}

void HubScanner::handle(const HubDetachedIOMessage &message) {}

} // namespace legowl
