#include "legowl/model/Hub.hpp"

#include <stdexcept>

namespace legowl {

void Hub::connect(std::uint8_t portId, std::unique_ptr<Peripheral> device) {
  auto &place = _devices[portId];
  if (place)
    throw std::invalid_argument(std::string{"Port "} + std::to_string(portId) +
                                " is already connected");

  place = std::move(device);
}

void Hub::disconnect(uint8_t portId)
{
  _devices.erase(portId);
}

} // namespace legowl
