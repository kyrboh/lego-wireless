#include "legowl/message/PortInformationRequest.hpp"

#include "legowl/util/Enum.hpp"

#include <cassert>

namespace legowl {

void MessageImpl<MessageType::PortInformationRequest>::serialize(Span<uint8_t> buffer) const {
  buffer[0] = _portId;
  buffer[1] = toUnderlyingType(_infoType);
}

std::string MessageImpl<MessageType::PortInformationRequest>::asString() const {
  std::string result = Message::asString() +
                       "::PortInformationRequestMessage{portId=" + std::to_string(_portId) +
                       ", infoType=" + std::string{toString(_infoType)} + "}";
  return result;
}

} // namespace legowl
