#include "legowl/message/GenericErrorMessages.hpp"

#include <cassert>

namespace legowl {

std::unique_ptr<Message> MessageImpl<MessageType::GenericErrorMessages>::read(
    CommonMessageHeader header,
    Span<const uint8_t> bytes) {
  constexpr std::size_t MESSAGE_SIZE = 5;
  assert(bytes.size() >= MESSAGE_SIZE);

  const std::uint8_t causingCommand = bytes[0];
  const auto errorCode = ErrorCode{bytes[1]};

  return std::make_unique<MessageImpl>(header, causingCommand, errorCode);
}

std::string MessageImpl<MessageType::GenericErrorMessages>::asString() const {
  return Message::asString() +
         "::GenericErrorMessage{causingCommand=" + std::to_string(_causingCommand) +
         ", errorCode=" + std::string{toString(_errorCode)} + "}";
}

} // namespace legowl
