#include "legowl/message/FWLockStatus.hpp"

#include <cassert>

namespace legowl {

std::unique_ptr<Message> MessageImpl<MessageType::FWLockStatus>::read(CommonMessageHeader header,
                                                                      Span<const uint8_t> bytes) {
  assert(bytes.size() >= 1);
  const auto lockStatus = static_cast<LockStatus>(bytes[0]);
  return std::make_unique<MessageImpl>(header, lockStatus);
}

std::string MessageImpl<MessageType::FWLockStatus>::asString() const
{
  std::string result = Message::asString() + "::MessageImpl{lockStatus=" +
                       std::string{toString(_lockStatus)} + "}";
  return result;
}

} // namespace legowl
