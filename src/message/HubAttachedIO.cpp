#include "legowl/message/HubAttachedIO.hpp"

#include <cassert>

namespace legowl {
namespace {
IOTypeID parseIoTypeId(Span<const std::uint8_t> bytes) {
  assert(bytes.size() >= sizeof(IOTypeID));

  const auto value = static_cast<std::uint16_t>((std::uint16_t{bytes[0]} << 8) | bytes[1]);
  return IOTypeID{value};
}

} // namespace

std::unique_ptr<Message> MessageImpl<MessageType::HubAttachedIO>::read(
    CommonMessageHeader header,
    Span<const std::uint8_t> bytes) {
  constexpr std::size_t COMMON_PART_SIZE = 2;
  assert(bytes.size() >= COMMON_PART_SIZE);

  const std::uint8_t portId = bytes[0];
  const auto eventType = HubAttachedIOEvent{bytes[1]};

  return route(eventType, [&](auto event) {
    return HubAttachedIOMessageImpl<event()>::read(header, portId, bytes.subspan(COMMON_PART_SIZE));
  });
}

std::string MessageImpl<MessageType::HubAttachedIO>::asString() const {
  std::string result = Message::asString() +
                       "::HubAttachedIOBasicMessage{portId=" + std::to_string(_portId) +
                       ", event=" + std::string{toString(_event)} + "}";
  return result;
}

std::unique_ptr<HubAttachedIOBasicMessage> HubAttachedIOMessageImpl<
    HubAttachedIOEvent::AttachedIO>::read(CommonMessageHeader header,
                                          uint8_t portId,
                                          Span<const std::uint8_t> bytes) {
  constexpr std::size_t IO_TYPE_SIZE = sizeof(std::uint16_t);
  constexpr std::size_t VERSION_SIZE = sizeof(std::int32_t);
  assert(bytes.size() >= IO_TYPE_SIZE + 2 * VERSION_SIZE);

  const IOTypeID ioTypeId = parseIoTypeId(bytes.subspan(0, IO_TYPE_SIZE));

  const Version hwRev = Version::read(bytes.subspan(IO_TYPE_SIZE, VERSION_SIZE));
  const Version swRev = Version::read(bytes.subspan(IO_TYPE_SIZE + VERSION_SIZE, VERSION_SIZE));

  return std::make_unique<HubAttachedIOMessageImpl>(header, portId, ioTypeId, hwRev, swRev);
}

std::string HubAttachedIOMessageImpl<HubAttachedIOEvent::AttachedIO>::asString() const {
  std::string result = HubAttachedIOBasicMessage::asString() +
                       "::HubAttachedIOMessage{ioTypeId=" + std::string{toString(_ioTypeId)} +
                       ", hwRev=" + _hardwareRevision.asFullString() +
                       ", swRev=" + _softwareRevision.asFullString() + "}";

  return result;
}

std::unique_ptr<HubAttachedIOBasicMessage> HubAttachedIOMessageImpl<
    HubAttachedIOEvent::AttachedVirtualIO>::read(CommonMessageHeader header,
                                                 uint8_t portId,
                                                 Span<const uint8_t> bytes) {
  constexpr std::size_t DATA_SIZE = 2 + 1 + 1;
  assert(bytes.size() >= DATA_SIZE);

  const IOTypeID ioTypeId = parseIoTypeId(bytes);
  const std::uint8_t portIdA = bytes[3];
  const std::uint8_t portIdB = bytes[4];

  return std::make_unique<HubAttachedIOMessageImpl>(header, portId, ioTypeId, portIdA, portIdB);
}

std::string HubAttachedIOMessageImpl<HubAttachedIOEvent::AttachedVirtualIO>::asString() const {
  std::string result = HubAttachedIOBasicMessage::asString() +
                       "::HubAttachedVirtualIOMessage{ioTypeId=" +
                       std::string{toString(_ioTypeId)} + ", portA=" + std::to_string(_portIdA) +
                       ", portIdB=" + std::to_string(_portIdB) + "}";
  return result;
}

std::unique_ptr<HubAttachedIOBasicMessage> HubAttachedIOMessageImpl<
    HubAttachedIOEvent::DetachedIO>::read(CommonMessageHeader header,
                                          uint8_t portId,
                                          Span<const uint8_t>) {
  return std::make_unique<HubAttachedIOMessageImpl>(header, portId);
}

std::string HubAttachedIOMessageImpl<HubAttachedIOEvent::DetachedIO>::asString() const {
  std::string result = HubAttachedIOBasicMessage::asString() + "::HubDetachedIOMessage{}";
  return result;
}

} // namespace legowl
