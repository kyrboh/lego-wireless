#include "legowl/message/HubAlerts.hpp"

#include <cassert>

namespace legowl {

std::unique_ptr<Message> MessageImpl<MessageType::HubAlerts>::read(CommonMessageHeader header,
                                                                   Span<const uint8_t> bytes) {
  constexpr std::size_t BASIC_MESSAGE_SIZE = 2;
  assert(bytes.size() >= BASIC_MESSAGE_SIZE);

  const auto alertType = HubAlertType{bytes[0]};
  const auto operation = HubAlertOperation{bytes[1]};

  bool isAlert = false;
  if (header.messageLength() > header.size() + BASIC_MESSAGE_SIZE)
    isAlert = static_cast<bool>(bytes[3]);

  return std::make_unique<MessageImpl>(header, alertType, operation, isAlert);
}

std::string MessageImpl<MessageType::HubAlerts>::asString() const {
  std::string result = Message::asString() +
                       "::HubAlertsMessage{alertType=" + std::string{toString(_alertType)} +
                       " operation=" + std::string{toString(_operation)} +
                       ", isAlert=" + std::to_string(_isAlert) + "}";

  return result;
}

} // namespace legowl
