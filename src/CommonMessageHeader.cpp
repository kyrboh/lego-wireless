#include "legowl/CommonMessageHeader.hpp"

#include "legowl/util/Enum.hpp"

#include <cassert>

namespace legowl {
namespace {

constexpr std::uint16_t MAX_ONE_BYTE_LENGTH = 127;
constexpr std::uint16_t LENGTH_ESCAPE_MASK = 0b1000'0000;

//! Decode length
//!
//! https://lego.github.io/lego-ble-wireless-protocol-docs/#len-encoding
std::uint16_t decodeLength(Span<const std::uint8_t> bytes) {
  std::uint16_t length = bytes[0];

  if (length & LENGTH_ESCAPE_MASK) {
    length &= ~LENGTH_ESCAPE_MASK;
    length |= bytes[1] << 7;
  }

  return length;
}

Span<std::uint8_t> encodeLength(Span<std::uint8_t> buffer, std::uint16_t length) {
  buffer[0] = static_cast<std::uint8_t>(length & ~LENGTH_ESCAPE_MASK);
  if (length <= MAX_ONE_BYTE_LENGTH)
    return buffer.subspan(1);

  buffer[0] |= LENGTH_ESCAPE_MASK;
  buffer[1] = static_cast<std::uint8_t>(length >> 7);

  return buffer.subspan(2);
}

} // namespace

CommonMessageHeader CommonMessageHeader::read(Span<const std::uint8_t> bytes)
{
  assert(bytes.size() >= REGULAR_SIZE);

  const std::uint16_t length = decodeLength(bytes.subspan(0, 2));

  const bool isExtended = length > MAX_ONE_BYTE_LENGTH;
  assert(!isExtended || bytes.size() >= REGULAR_SIZE);

  std::size_t offset = isExtended ? 2 : 1;

  // NOTE: not used, should be set to 0
  const std::uint8_t hubId = bytes[offset++];
  const MessageType messageType = MessageType{bytes[offset++]};
  assert(offset < bytes.size());

  return CommonMessageHeader{length, hubId, messageType};
}

Span<uint8_t> CommonMessageHeader::write(Span<std::uint8_t> bytes) const
{
  assert(bytes.size() >= messageLength());

  auto buffer = encodeLength(bytes, _length);
  buffer[0] = _hubId;
  buffer[1] = toUnderlyingType(_messageType);

  return buffer.subspan(2);
}

std::size_t CommonMessageHeader::size() const {
  return messageLength() > MAX_ONE_BYTE_LENGTH ? EXTENDED_SIZE : REGULAR_SIZE;
}

} // namespace legowl
