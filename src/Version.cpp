#include "legowl/Version.hpp"

#include <cassert>

namespace legowl {

Version Version::read(Span<const uint8_t> bytes) {
  assert(bytes.size() >= sizeof(std::int32_t));

  const std::uint8_t major = (bytes[0] & 0b1111'0000) >> 4;
  const std::uint8_t minor = bytes[0] & 0b0000'1111;
  const std::uint8_t patch = bytes[1];
  const std::uint16_t build = static_cast<std::uint16_t>(bytes[3] << 8 | bytes[3]);

  return Version{major, minor, patch, build};
}

std::string Version::asString() const {
  // the build version should not be included by default
  std::string result = std::to_string(_major) + '.' + std::to_string(_minor) + '.' +
                       std::to_string(_patch);
  return result;
}

std::string Version::asFullString() const { return asString() + '.' + std::to_string(_build); }

} // namespace legowl
