#include <legowl/util/CompilerQuirks.hpp>
#include <legowl/util/Demangle.hpp>

#if LEGOWL_COMPILER == LEGOWL_COMPILER_GCC || LEGOWL_COMPILER == LEGOWL_COMPILER_CLANG
#  include <cxxabi.h>

#  include <cstdlib>
#  include <memory>

namespace legowl {

std::string demangle(const char *mangled) {
  int status = 0;

  std::unique_ptr<char, decltype(&std::free)> chars{
      abi::__cxa_demangle(mangled, nullptr, nullptr, &status),
      std::free};

  return status == 0 && chars ? chars.get() : mangled;
}

} // namespace legowl
#else

std::string legowl::demagle(const char *mangled) { return mangled; }

#endif
