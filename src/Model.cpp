#include <legowl/Model.hpp>

#include <stdexcept>
#include <string>

namespace legowl {

namespace {
template <typename Hubs>
auto &getHub(Hubs &hubs, std::uint8_t hubId) {
  auto it = hubs.find(hubId);
  if (it == hubs.end())
    throw std::out_of_range("there is no hub with id " + std::to_string(hubId));

  return *it->second;
}
} // namespace

void Model::addHub(std::uint8_t hubId, std::unique_ptr<Hub> hub) {
  auto &place = _hubs[hubId];
  if (place)
    throw std::invalid_argument(std::string{"hub with id "} + std::to_string(hubId) +
                                " already exists");

  place = std::move(hub);
}

bool Model::hasHub(uint8_t hubId) const { return _hubs.count(hubId) > 0; }

void Model::removeHub(uint8_t hubId) { _hubs.erase(hubId); }

Hub &Model::hub(std::uint8_t hubId) { return getHub(_hubs, hubId); }

const Hub &Model::hub(uint8_t hubId) const { return getHub(_hubs, hubId); }

} // namespace legowl
