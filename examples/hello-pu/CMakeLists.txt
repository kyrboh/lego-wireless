project(hello-pu CXX)

find_package(TinyB REQUIRED)

include_directories(${TINYB_INCLUDE_DIR})
include_directories(${LEGOWL_INCLUDE_DIR})

find_package(Threads)

add_executable(${PROJECT_NAME} main.cpp)
target_link_libraries(${PROJECT_NAME} ${TINYB_LIBRARIES} legowl ${CMAKE_THREAD_LIBS_INIT})
