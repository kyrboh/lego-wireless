#include <legowl/CommonMessageHeader.hpp>
#include <legowl/HubScanner.hpp>
#include <legowl/Message.hpp>
#include <legowl/model/Hub.hpp>
#include <legowl/util/Visitor.hpp>
#include <tinyb/BluetoothDevice.hpp>
#include <tinyb/BluetoothException.hpp>
#include <tinyb/BluetoothManager.hpp>

#include <atomic>
#include <bitset>
#include <iomanip>
#include <iostream>
#include <mutex>
#include <thread>

using namespace std::chrono_literals;

struct HubDevice {
  legowl::Hub hub;
  std::unique_ptr<tinyb::BluetoothDevice> device;
};

void speak(legowl::Hub &hub,
           tinyb::BluetoothGattCharacteristic &chr,
           const std::atomic<bool> &isEndRequested) {
  legowl::HubScanner hubScanner(hub);

  chr.enable_value_notifications([&hubScanner](const std::vector<std::uint8_t> &value) {
    legowl::Span bytes{std::as_const(value)};
    while (!bytes.empty()) {
      auto message = legowl::Message::read(bytes);
      hubScanner.process(*message);
      bytes = bytes.subspan(message->length());
    }
  });

  while (!isEndRequested.load())
    std::this_thread::sleep_for(std::chrono::seconds{1});

  chr.disable_value_notifications();
}

std::vector<HubDevice> scan(tinyb::BluetoothManager &btMgr) {
  std::vector<HubDevice> hubs;

  std::cout << "Discovering Technic Hub...\n";
  constexpr int retries = 5;

  for (int i = 0; i < retries; ++i) {
    std::vector<std::unique_ptr<tinyb::BluetoothDevice>> devices = btMgr.get_devices();

    std::cout << ">>> Attempt #" << i << '\n';

    for (std::unique_ptr<tinyb::BluetoothDevice> &device : devices) {
      // TODO: the name isn't always there, ask the user?
      const std::string name = device->get_name();

      if (name == "Technic Hub" && (device->get_rssi() != 0 || device->get_connected())) {
        std::cout << "Found '" << name << "' at " << device->get_address() << '\n'
                  << "  alias: " << device->get_alias() << '\n'
                  << "  connected: " << device->get_connected() << '\n'
                  << "  paired: " << device->get_paired() << '\n'
                  << "  signal: " << device->get_rssi() << '\n';

        hubs.emplace_back(HubDevice{legowl::Hub{}, std::move(device)});
      }
    }

    std::cout << "discovered total " << hubs.size() << " Technic Hub(s)\n" << std::flush;
    std::cout << '\n';
    if (hubs.size() > 0)
      break;

    std::this_thread::sleep_for(3s);
  }

  return hubs;
}

int main() {
  std::cout << "Attempt at LEGO bluetooth\n";

  auto *btMgr = tinyb::BluetoothManager::get_bluetooth_manager();



  if (btMgr->start_discovery())
    std::cout << "Discovery started\n";
  else
    std::cerr << "error: start_discovery\n";

  auto hubs = scan(*btMgr);

  std::cout << "Stopping discovery...\n";
  if (!btMgr->stop_discovery())
    std::cerr << "error: stop_discovery\n";

  for (auto &[hub, device] : hubs) {
    std::cout << "- Path:      " << device->get_object_path() << '\n'
              << "  Name:      " << device->get_name() << '\n'
              << "  Connected: " << device->get_connected() << '\n'
              << "  Address:   " << device->get_address() << '\n'
              << std::flush;

    try {
      const bool wasAlreadyConnected = device->get_connected();
      if (!wasAlreadyConnected) {
        if (!device->connect()) {
          std::cout << "Cannot connect to hub: " << device->get_address() << '\n';
          continue;
        }
      }

      std::vector<std::unique_ptr<tinyb::BluetoothGattService>> services = device->get_services();
      std::cout << "Connected to the Hub. Listing " << services.size() << " service(s)\n";

      std::unique_ptr<tinyb::BluetoothGattCharacteristic> characteristic;

      for (const std::unique_ptr<tinyb::BluetoothGattService> &service : services) {
        std::cout << "Service: " << service->get_uuid() << " (primary: " << service->get_primary()
                  << ")\n";
        std::vector<std::unique_ptr<tinyb::BluetoothGattCharacteristic>> characteristics =
            service->get_characteristics();

        for (std::unique_ptr<tinyb::BluetoothGattCharacteristic> &chr : characteristics) {
          std::cout << "  Characteristic: " << chr->get_uuid() << '\n';
          std::vector<std::string> flags = chr->get_flags();
          for (const std::string &flag : flags) {
            std::cout << "    Flag: " << flag << '\n';
          }

          if (chr->get_uuid() == "00001624-1212-efde-1623-785feabcd123") {
            characteristic = std::move(chr);
          }
        }
      }

      std::map<std::string, std::vector<std::uint8_t>> svcData = device->get_service_data();
      std::cout << "Service data:\n";
      for (const auto &[key, value] : svcData) {
        std::cout << "  " << key << ": ";
        for (std::uint8_t byte : value) {
          std::cout << std::hex << std::setw(2) << std::setfill('0') << unsigned{byte} << ' ';
        }
        std::cout << '\n';
      }

      std::map<std::uint16_t, std::vector<std::uint8_t>> mfgData = device->get_manufacturer_data();
      std::cout << "Manufacturer data:\n";
      for (const auto &[mfgId, value] : mfgData) {
        constexpr std::uint16_t LEGO_MFG = 0x0397;
        if (mfgId == LEGO_MFG && value.size() >= 6) {
          std::cout << "  Button state: " << unsigned{value[0]} << '\n';

          auto systemType = std::bitset<3>((value[1] & 0b11100000) >> 5);
          auto deviceNo = std::bitset<5>(value[1] & 0b00011111);
          std::cout << "  System Type & Device Number: " << systemType << ' ' << deviceNo << '\n';

          std::cout << "  Device Capabilities: " << std::hex << std::setw(2) << std::setfill('0')
                    << value[2] << '\n';
          std::cout << "  Last network: " << std::dec << unsigned{value[3]} << '\n';
          std::cout << "  Status: " << std::dec << unsigned{value[4]} << '\n';
          std::cout << "  Option: " << std::dec << unsigned{value[5]} << '\n';
        } else {
          std::cout << "  " << std::hex << std::setw(4) << std::setfill('0') << mfgId << ' ';

          for (std::uint8_t byte : value) {
            std::cout << std::hex << std::setw(2) << std::setfill('0') << unsigned{byte} << ' ';
          }
          std::cout << '\n';
        }
      }

      if (characteristic) {
        std::cout << "Speaking with the hub\n";

        std::atomic<bool> isEndRequested = false;
        std::cout << "Waiting for input to finish...\n" << std::flush;
        std::thread speaker(speak,
                            std::ref(hub),
                            std::ref(*characteristic),
                            std::ref(isEndRequested));

        std::cin.get();
        std::cout << "Stopping...\n" << std::flush;

        isEndRequested = true;
        if (speaker.joinable())
          speaker.join();
      }

      if (!wasAlreadyConnected) {
        if (!device->disconnect()) {
          std::cout << "Failed to disconnect from hub: " << device->get_address() << '\n';
        }
      }
      std::cout << '\n';
    } catch (const tinyb::BluetoothException &e) {
      std::cout << device->get_address() << ": hub connection failed: " << e.what() << '\n';
    }
  }

  return 0;
}
