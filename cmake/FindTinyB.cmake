# - Try to find the TinyB library
# Once done this defines
#
#  TINYB_FOUND - system has libbluetooth
#  TINYB_INCLUDE_DIR - the libbluetooth include directory
#  TINYB_LIBRARIES - Link these to use libbluetooth

if (TINYB_INCLUDE_DIR AND TINYB_LIBRARIES)
  # in cache already
  set(LIBBLUETOOTH_FOUND TRUE)
else ()
  if (NOT WIN32)
    find_package(PkgConfig)
    pkg_check_modules(PC_TINYB tinyb)
  endif()

  find_path(TINYB_INCLUDE_DIR tinyb.hpp
    PATHS ${PC_TINYB_INCLUDEDIR} ${PC_TINYB_INCLUDE_DIRS}}
    PATHS /usr/include /usr/local/include)

  find_library(TINYB_LIBRARIES
    NAMES tinyb
    PATHS ${PC_TINYB_INCLUDEDIR_LIBDIR} ${PC_TINYB_LIBRARY_DIRS}
    PATHS /usr/lib)

  include(FindPackageHandleStandardArgs)
  find_package_handle_standard_args(TINYB DEFAULT_MSG
    TINYB_LIBRARIES
    TINYB_INCLUDE_DIR)

  mark_as_advanced(TINYB_LIBRARIES TINYB_INCLUDE_DIR)
endif()
