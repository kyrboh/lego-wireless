LEGO Wireless Library
=====================

This library is intended to provide a useful set of abstractions for communicating over the [LEGO Wireless Protocol](https://lego.github.io/lego-ble-wireless-protocol-docs/).

Building
--------

No specific instructions (yet). Just follow the usual CMake mantra:

```
mkdir build && cd build
cmake ..
make
```

### GNU/Linux x86_64

Development process is happening in a modern x86_64 GNU/Linux environment. So this is the primary target.

### Raspberry Pi

Ideally, the device is the perfect candidate for using this library. However, due to technical complications (i.e. Raspbian's ancient toolchain and glibc) there is no (obvious) way to build it. So RPi support is yet to come. Stay tuned.

### Other Platforms

No plans.

Dependencies
------------

### TinyB

TinyB's sources are hosted on [Intel's GitHub](https://github.com/intel-iot-devkit/tinyb). If your environment does not provide TinyB, consider building and packaging using CPack. 
