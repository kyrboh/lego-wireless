#pragma once

#include <legowl/model/Hub.hpp>

#include <cstdint>
#include <memory>
#include <unordered_map>

namespace legowl {

//! Represents a functioning model
class Model {
public:
  explicit Model() = default;

  void addHub(std::uint8_t hubId, std::unique_ptr<Hub> hub);
  bool hasHub(std::uint8_t hubId) const;
  void removeHub(std::uint8_t hubId);

  Hub &hub(std::uint8_t hubId);
  const Hub &hub(std::uint8_t hubId) const;

private:
  std::unordered_map<std::uint8_t, std::unique_ptr<Hub>> _hubs;
};

} // namespace legowl
