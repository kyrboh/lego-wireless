#pragma once

#include <legowl/enum/HubAttachedIOEvent.hpp>
#include <legowl/enum/MessageType.hpp>

namespace legowl {
//! Implementations are instantiations of this
template <MessageType>
class MessageImpl;

using HubAttachedIOBasicMessage = MessageImpl<MessageType::HubAttachedIO>;
using FWLockStatusMessage = MessageImpl<MessageType::FWLockStatus>;
using GenericErrorMessage = MessageImpl<MessageType::GenericErrorMessages>;
using HubAlertsMessage = MessageImpl<MessageType::HubAlerts>;
using PortInformationRequestMessage = MessageImpl<MessageType::PortInformationRequest>;

template <HubAttachedIOEvent>
class HubAttachedIOMessageImpl;

using HubAttachedIOMessage = HubAttachedIOMessageImpl<HubAttachedIOEvent::AttachedIO>;
using HubAttachedVirtualIOMessage = HubAttachedIOMessageImpl<HubAttachedIOEvent::AttachedVirtualIO>;
using HubDetachedIOMessage = HubAttachedIOMessageImpl<HubAttachedIOEvent::DetachedIO>;

} // namespace legowl
