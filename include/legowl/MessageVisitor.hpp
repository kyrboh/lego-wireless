#pragma once

#include <legowl/Message.hpp>
#include <legowl/Messages.hpp>
#include <legowl/util/TypeTraits.hpp>

#include <utility>

namespace legowl {

//! Invoke callback with an type-specific underlying message
template <typename Callback>
auto visit(const Message& msg, Callback callback) {
  return route(msg.type(), [&](auto payloadType) {
    using MessageType = MessageImpl<payloadType>;
    if constexpr (isCompleteType<MessageType>) {
      callback(static_cast<const MessageType&>(msg));
    }
  });
}

} // namespace legowl
