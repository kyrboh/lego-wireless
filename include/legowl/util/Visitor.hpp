#pragma once

namespace legowl {

template <typename... Functors>
class Visitor : public Functors... {
public:
  using Functors::operator()...;
};

template <typename... Functors>
Visitor(Functors...) -> Visitor<Functors...>;

} // namespace legowl
