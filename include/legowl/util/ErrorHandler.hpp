#pragma once

#include <legowl/util/Enum.hpp>
#include <legowl/util/TypeName.hpp>

#include <cstdint>
#include <stdexcept>
#include <string>

namespace legowl {
namespace throwing {
template <typename T>
[[noreturn]] void unknownEnumValue(T value) {
  throw std::invalid_argument(typeName<T>() +
                              ": unknown value: " + std::to_string(toUnderlyingType(value)));
}
} // namespace throwing
} // namespace legowl
