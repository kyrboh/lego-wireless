#pragma once

#include <array>
#include <cstddef>
#include <cstdint>
#include <iterator>
#include <memory>
#include <type_traits>

namespace legowl {

inline constexpr std::size_t DYNAMIC_EXTENT = -1;

//! A compatible replacement of std::span until we get C++20
template <typename T, std::size_t Extent = DYNAMIC_EXTENT>
class Span {
  static_assert(Extent == DYNAMIC_EXTENT, "static Span is not implemented");
};

template <typename T>
class Span<T, DYNAMIC_EXTENT> {
public:
  using element_type = T;
  using value_type = std::remove_cv_t<T>;
  using size_type = std::size_t;
  using difference_type = std::ptrdiff_t;
  using pointer = T *;
  using const_pointer = const T *;
  using reference = T &;
  using const_reference = const T &;
  using iterator = pointer;
  using reverse_iterator = std::reverse_iterator<iterator>;

  static constexpr std::size_t extent = DYNAMIC_EXTENT;

  constexpr Span() noexcept = default;

  // NOTE: std::span uses std::to_address instead, but it is part of C++20
  template <typename Iterator>
  constexpr Span(Iterator it, size_type count) : _data{std::addressof(*it)}, _size{count} {}

  template <typename Begin, typename End>
  constexpr Span(Begin begin, End end) : Span(begin, static_cast<size_type>(end - begin)) {}

  template <std::size_t N>
  constexpr Span(element_type (&arr)[N]) noexcept : _data{std::data(arr)}, _size{N} {}

  template <typename U, std::size_t N>
  constexpr Span(std::array<U, N> &arr) noexcept : _data{arr.data()}, _size{arr.size()} {}

  template <typename U, std::size_t N>
  constexpr Span(const std::array<U, N> &arr) noexcept : _data{arr.data()}, _size{arr.size()} {}

  template <typename Range>
  constexpr Span(Range &&range) : _data{std::data(range)}, _size{std::size(range)} {}

  template <typename U, std::size_t N>
  constexpr Span(const Span<U, N> &other) noexcept : _data{other._data}, _size{other._size} {}

  constexpr Span(const Span &other) = default;

  constexpr Span &operator=(const Span &other) noexcept = default;

  constexpr iterator begin() const noexcept { return _data; }
  constexpr iterator end() const noexcept { return begin() + _size; }

  constexpr reverse_iterator rbegin() const noexcept { return reverse_iterator(end()); }
  constexpr reverse_iterator rend() const noexcept { return reverse_iterator(begin()); }

  constexpr reference front() const { return *begin(); }
  constexpr reference back() const { return *(end() - 1); }

  constexpr reference operator[](size_type idx) const { return data()[idx]; }

  constexpr pointer data() const noexcept { return _data; }

  constexpr size_type size() const noexcept { return _size; }
  constexpr size_type size_bytes() const noexcept { return size() * sizeof(element_type); }

  [[nodiscard]] constexpr bool empty() const noexcept { return size() == 0; }

  template <std::size_t Count>
  constexpr Span<element_type, Count> first() const = delete;

  constexpr Span first(std::size_t count) const { return Span{_data, count}; }

  template <std::size_t Count>
  constexpr Span<element_type, Count> last() const = delete;

  constexpr Span last(std::size_t count) const { return Span{end() - count, count}; }

  template <std::size_t Offset, std::size_t Count = DYNAMIC_EXTENT>
  constexpr Span<element_type, Count> subspan() const = delete;

  constexpr Span subspan(std::size_t offset, std::size_t count = DYNAMIC_EXTENT) const {
    if (count == DYNAMIC_EXTENT)
      return Span{_data + offset, _size - offset};

    return Span{_data + offset, count};
  }

private:
  pointer _data = nullptr;
  size_type _size = 0;
};

template <typename T, std::size_t N>
auto as_bytes(Span<T, N> s) noexcept {
  constexpr std::size_t extent = N == DYNAMIC_EXTENT ? DYNAMIC_EXTENT : N * sizeof(T);
  return Span<const std::byte *, extent>(reinterpret_cast<const std::byte *>(s.data()),
                                         s.size_bytes());
}

template <typename T, std::size_t N>
auto as_writable_bytes(Span<T, N> s) noexcept {
  constexpr std::size_t extent = N == DYNAMIC_EXTENT ? DYNAMIC_EXTENT : N * sizeof(T);
  return Span<std::byte *, extent>(reinterpret_cast<std::byte *>(s.data()), s.size_bytes());
}

template <typename Range>
Span(const Range &)
    -> Span<const typename std::remove_reference_t<Range>::value_type, DYNAMIC_EXTENT>;

template <typename T, std::size_t N>
Span(T (&)[N]) -> Span<T, N>;

} // namespace legowl
