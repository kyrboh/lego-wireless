#pragma once

#include <type_traits>

namespace legowl {

template <typename Enum>
auto toUnderlyingType(Enum value) {
  static_assert(std::is_enum_v<Enum>);
  return static_cast<std::underlying_type_t<Enum>>(value);
}

} // namespace legowl
