#pragma once

#include <type_traits>

namespace legowl {

template <auto value>
using constant = std::integral_constant<decltype(value), value>;

} // namespace legowl
