#pragma once

#include <legowl/util/Demangle.hpp>

#include <string>
#include <typeinfo>

namespace legowl {

template <typename T>
std::string typeName() {
  // TODO: need to avoid calling typeid if RTTI is off
  return demangle(typeid(T).name());
}

} // namespace legowl
