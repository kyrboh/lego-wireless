#pragma once

#include <cstdint>

#define LEGOWL_COMPILER_UNKNOWN 0
#define LEGOWL_COMPILER_GCC     1
#define LEGOWL_COMPILER_CLANG   2

#if __GNUC__
#  define LEGOWL_COMPILER LEGOWL_COMPILER_GCC
#elif __clang__
#  define LEGOWL_COMPILER LEGOWL_COMPILER_CLANG
#else
#  define LEGOWL_COMPILER LEGOWL_COMPILER_UNKNOWN
#endif

#if LEGOWL_COMPILER == LEGOWL_COMPILER_CLANG || LEGOWL_COMPILER == LEGOWL_COMPILER_GCC
#  define UNREACHABLE __builtin_unreachable()
#else
#  define UNREACHABLE
#endif

namespace legowl {

enum class Compiler : std::uint8_t {
  Unknown = LEGOWL_COMPILER_UNKNOWN,
  GCC = LEGOWL_COMPILER_GCC,
  Clang = LEGOWL_COMPILER_CLANG,
};

inline constexpr Compiler compiler{LEGOWL_COMPILER};

} // namespace legowl
