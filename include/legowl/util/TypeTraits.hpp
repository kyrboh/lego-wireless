#pragma once

//! \file Various utilities to complement <type_traits>

#include <type_traits>

namespace legowl {
// IMPORTANT: anonymous namespace is intended, to reduce the chance of breaking the ODR
namespace {
template <typename T>
class IsCompleteTypeHelper {
  template <typename U>
  static std::bool_constant<sizeof(U) == sizeof(U)> check(U *);
  static std::false_type check(...);

public:
  using type = decltype(check(static_cast<T *>(nullptr)));
};

template <typename T>
using IsCompleteType = typename IsCompleteTypeHelper<T>::type;

template <typename T>
constexpr bool isCompleteType = IsCompleteType<T>::value;
} // namespace

struct NoneSuch {
  ~NoneSuch() = delete;
  NoneSuch(NoneSuch const&) = delete;
  void operator=(NoneSuch const&) = delete;
};

namespace detail {
template <typename Default,
          typename AlwaysVoid,
          template <typename...>
          typename Op,
          typename... Args>
struct Detector {
  using Value = std::false_type;
  using Type = Default;
};

template <typename Default, template <typename...> class Op, typename... Args>
struct Detector<Default, std::void_t<Op<Args...>>, Op, Args...> {
  using Value = std::true_type;
  using Type = Op<Args...>;
};
} // namespace detail

template <template <typename...> typename Op, typename... Args>
using IsDetected = typename detail::Detector<NoneSuch, void, Op, Args...>::Value;

template <template <typename...> typename Op, typename... Args>
constexpr bool isDetected = IsDetected<Op, Args...>::value;

template <template <typename...> typename Op, typename... Args>
using Detected = typename detail::Detector<NoneSuch, void, Op, Args...>::Type;

template <typename Default, template <typename...> typename Op, typename... Args>
using DetectedOr = detail::Detector<Default, void, Op, Args...>;

} // namespace legowl
