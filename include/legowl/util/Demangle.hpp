#pragma once

#include <string>

namespace legowl {

std::string demangle(const char *mangled);

} // namespace legowl
