#pragma once

#include <legowl/enum/MessageType.hpp>
#include <legowl/util/Span.hpp>

#include <cstdint>

namespace legowl {

//! All messages use this header
//!
//! https://lego.github.io/lego-ble-wireless-protocol-docs/#common-message-header
class CommonMessageHeader {
public:
  constexpr static std::size_t REGULAR_SIZE = 3;
  constexpr static std::size_t EXTENDED_SIZE = REGULAR_SIZE + 1;

  explicit CommonMessageHeader(std::uint16_t length, std::uint8_t hubId, MessageType messageType) :
      _length{length},
      _hubId{hubId},
      _messageType{messageType} {}

  static CommonMessageHeader read(Span<const std::uint8_t> bytes);

  std::uint16_t messageLength() const { return _length; }
  MessageType messageType() const { return _messageType; }

  //! Write header and return view for payload to be written
  Span<uint8_t> write(Span<std::uint8_t> bytes) const;

  //! The size of this header in bytes
  std::size_t size() const;

private:
  std::uint16_t _length;
  std::uint8_t _hubId;
  MessageType _messageType;
};

} // namespace legowl
