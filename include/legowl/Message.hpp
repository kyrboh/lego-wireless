#pragma once

#include <legowl/CommonMessageHeader.hpp>
#include <legowl/MessageFwd.hpp>
#include <legowl/enum/MessageType.hpp>

#include <cstdint>
#include <memory>

namespace legowl {

class Message {
public:
  static std::unique_ptr<Message> read(Span<const std::uint8_t> bytes);

  std::uint16_t length() const { return _header.messageLength(); }
  MessageType type() const { return _header.messageType(); }

  void write(Span<std::uint8_t> buffer) const;

  virtual std::string asString() const;

  virtual ~Message() = default;

protected:
  explicit Message(CommonMessageHeader header) : _header(header) {}

private:
  virtual void serialize(Span<std::uint8_t> buffer) const;

  CommonMessageHeader _header;
};

} // namespace legowl
