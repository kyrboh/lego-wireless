#pragma once

#include <legowl/CommonMessageHeader.hpp>
#include <legowl/Message.hpp>
#include <legowl/enum/MessageType.hpp>
#include <legowl/util/Span.hpp>
#include <legowl/enum/ErrorCode.hpp>

#include <cstdint>
#include <memory>

namespace legowl {

//! Error messages
//!
//! https://lego.github.io/lego-ble-wireless-protocol-docs/index.html#generic-error-messages
template <>
class MessageImpl<MessageType::GenericErrorMessages> : public Message {
public:
  explicit MessageImpl(CommonMessageHeader header,
                       std::uint8_t causingCommand,
                       ErrorCode errorCode) :
      Message(header),
      _causingCommand{causingCommand},
      _errorCode{errorCode} {}

  static std::unique_ptr<Message> read(CommonMessageHeader header, Span<const std::uint8_t> bytes);

  std::uint8_t causingCommand() const { return _causingCommand; }
  ErrorCode errorCode() const { return _errorCode; }

  std::string asString() const override;

private:
  std::uint8_t _causingCommand; // TODO: enum
  ErrorCode _errorCode;
};

} // namespace legowl
