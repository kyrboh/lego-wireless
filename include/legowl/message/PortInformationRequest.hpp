#pragma once

#include <legowl/Message.hpp>
#include <legowl/enum/MessageType.hpp>
#include <legowl/enum/PortInformationType.hpp>
#include <legowl/util/Span.hpp>

#include <memory>
#include <string>

namespace legowl {

//! Request information about a port
//!
//! https://lego.github.io/lego-ble-wireless-protocol-docs/index.html#port-information-request
template <>
class MessageImpl<MessageType::PortInformationRequest> : public Message {
public:
  explicit MessageImpl(CommonMessageHeader header,
                       std::uint8_t portId,
                       PortInformationType infoType) :
      Message(header),
      _portId{portId},
      _infoType{infoType} {}

  explicit MessageImpl(std::uint8_t hubId, std::uint8_t portId, PortInformationType infoType) :
      MessageImpl(CommonMessageHeader{MESSAGE_SIZE, hubId, MessageType::PortInformationRequest},
                  portId,
                  infoType) {}

  static std::unique_ptr<Message> read(CommonMessageHeader, Span<const std::uint8_t>) = delete;

  std::string asString() const override;

private:
  constexpr static std::size_t MESSAGE_SIZE = CommonMessageHeader::REGULAR_SIZE + 2;

  void serialize(Span<std::uint8_t> buffer) const override;

  std::uint8_t _portId;
  PortInformationType _infoType;
};

} // namespace legowl
