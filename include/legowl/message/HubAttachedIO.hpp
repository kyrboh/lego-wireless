#pragma once

#include <legowl/Message.hpp>
#include <legowl/MessageFwd.hpp>
#include <legowl/Version.hpp>
#include <legowl/enum/HubAttachedIOEvent.hpp>
#include <legowl/enum/IOTypeID.hpp>
#include <legowl/enum/MessageType.hpp>
#include <legowl/enum/PortClass.hpp>

#include <cstdint>
#include <memory>

namespace legowl {
//! https://lego.github.io/lego-ble-wireless-protocol-docs/index.html#hub-attached-i-o
template <>
class MessageImpl<MessageType::HubAttachedIO> : public Message {
public:
  static std::unique_ptr<Message> read(CommonMessageHeader header, Span<const std::uint8_t> bytes);

  std::uint8_t portId() const { return _portId; }
  HubAttachedIOEvent event() const { return _event; }
  PortClass portClass() const { return toPortClass(_portId); }

  //! Invoke callback with an event-specific underlying message
  template <typename Callback>
  auto visit(Callback callback) const {
    return route(_event, [&](auto event) {
      using Reference = const HubAttachedIOMessageImpl<event()> &;
      return callback(static_cast<Reference>(*this));
    });
  }

  std::string asString() const override;

protected:
  MessageImpl(CommonMessageHeader header, std::uint8_t portId, HubAttachedIOEvent event) :
      Message{header},
      _portId{portId},
      _event{event} {}

private:
  std::uint8_t _portId;
  HubAttachedIOEvent _event;
};

template <>
class HubAttachedIOMessageImpl<HubAttachedIOEvent::AttachedIO> : public HubAttachedIOBasicMessage {
public:
  HubAttachedIOMessageImpl(CommonMessageHeader header,
                           std::uint8_t portId,
                           IOTypeID ioTypeId,
                           Version hwRev,
                           Version swRev) :
      HubAttachedIOBasicMessage(header, portId, HubAttachedIOEvent::AttachedIO),
      _ioTypeId{ioTypeId},
      _hardwareRevision{hwRev},
      _softwareRevision{swRev} {}

  static std::unique_ptr<HubAttachedIOBasicMessage> read(CommonMessageHeader header,
                                                         std::uint8_t portId,
                                                         Span<const std::uint8_t> bytes);

  IOTypeID ioTypeId() const { return _ioTypeId; }
  Version hardwareRevision() const { return _hardwareRevision; }
  Version softwareRevision() const { return _softwareRevision; }

  std::string asString() const override;

private:
  IOTypeID _ioTypeId;
  Version _hardwareRevision;
  Version _softwareRevision;
};

template <>
class HubAttachedIOMessageImpl<HubAttachedIOEvent::AttachedVirtualIO> :
    public HubAttachedIOBasicMessage {
public:
  HubAttachedIOMessageImpl(CommonMessageHeader header,
                           std::uint8_t portId,
                           IOTypeID ioTypeId,
                           std::uint8_t portIdA,
                           std::uint8_t portIdB) :
      HubAttachedIOBasicMessage(header, portId, HubAttachedIOEvent::AttachedVirtualIO),
      _ioTypeId{ioTypeId},
      _portIdA{portIdA},
      _portIdB{portIdB} {}

  static std::unique_ptr<HubAttachedIOBasicMessage> read(CommonMessageHeader header,
                                                         std::uint8_t portId,
                                                         Span<const std::uint8_t> bytes);

  IOTypeID ioTypeId() const { return _ioTypeId; }

  std::uint8_t portIdA() const { return _portIdA; }
  std::uint8_t portIdB() const { return _portIdB; }

  std::string asString() const override;

private:
  IOTypeID _ioTypeId;
  std::uint8_t _portIdA;
  std::uint8_t _portIdB;
};

template <>
class HubAttachedIOMessageImpl<HubAttachedIOEvent::DetachedIO> : public HubAttachedIOBasicMessage {
public:
  explicit HubAttachedIOMessageImpl(CommonMessageHeader header, std::uint8_t portId) :
      HubAttachedIOBasicMessage(header, portId, HubAttachedIOEvent::DetachedIO) {}

  static std::unique_ptr<HubAttachedIOBasicMessage> read(CommonMessageHeader header,
                                                         std::uint8_t portId,
                                                         Span<const std::uint8_t> bytes);

  std::string asString() const override;
};

} // namespace legowl
