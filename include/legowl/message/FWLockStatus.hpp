#pragma once

#include <legowl/CommonMessageHeader.hpp>
#include <legowl/Message.hpp>
#include <legowl/enum/LockStatus.hpp>
#include <legowl/enum/MessageType.hpp>
#include <legowl/util/Span.hpp>

#include <memory>
#include <string>

namespace legowl {

//! Report of memory locking for firmware update
//!
//! https://lego.github.io/lego-ble-wireless-protocol-docs/index.html#f-w-lock-status
template <>
class MessageImpl<MessageType::FWLockStatus> : public Message {
public:
  explicit MessageImpl(CommonMessageHeader header, LockStatus lockStatus) :
      Message(header),
      _lockStatus{lockStatus} {}

  static std::unique_ptr<Message> read(CommonMessageHeader header, Span<const std::uint8_t> bytes);

  LockStatus lockStatus() const { return _lockStatus; }

  std::string asString() const override;

private:
  LockStatus _lockStatus;
};

} // namespace legowl
