#pragma once

#include <legowl/Message.hpp>
#include <legowl/enum/HubAlertOperation.hpp>
#include <legowl/enum/HubAlertType.hpp>
#include <legowl/enum/MessageType.hpp>
#include <legowl/util/Span.hpp>

#include <cstdint>
#include <memory>
#include <string>

namespace legowl {

template <>
class MessageImpl<MessageType::HubAlerts> : public Message {
public:
  explicit MessageImpl(CommonMessageHeader header,
                       HubAlertType alertType,
                       HubAlertOperation operation,
                       bool isAlert) :
      Message(header),
      _alertType{alertType},
      _operation{operation},
      _isAlert{isAlert} {}

  static std::unique_ptr<Message> read(CommonMessageHeader header, Span<const std::uint8_t> bytes);

  HubAlertType alertType() const { return _alertType; }
  HubAlertOperation operation() const { return _operation; }
  bool isAlert() const { return _isAlert; }

  std::string asString() const override;

private:
  HubAlertType _alertType;
  HubAlertOperation _operation;
  bool _isAlert;
};

} // namespace legowl
