#pragma once

#include <legowl/Message.hpp>
#include <legowl/MessageFwd.hpp>

#include <memory>
#include <deque>
#include <queue>

namespace legowl {
class Hub;

class HubScanner {
public:
  explicit HubScanner(Hub &output) : _hub{&output} {}

  //! Process a read Message
  void process(const Message &message);

  std::size_t outboxSize() const { return _outbox.size(); }

  //! Get a message that ModelScanner has at top of the queue
  std::unique_ptr<Message> poll();

private:
  [[noreturn]] void handle(const Message &unknownMessage);

  void handle(const HubAttachedIOBasicMessage& message);
  void handle(const HubAttachedIOMessage& message);
  void handle(const HubAttachedVirtualIOMessage& message);
  void handle(const HubDetachedIOMessage& message);

  Hub *_hub;
  std::queue<std::unique_ptr<Message>> _outbox;
};

} // namespace legowl
