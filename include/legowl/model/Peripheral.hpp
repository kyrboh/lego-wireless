#pragma once

#include <legowl/enum/IOTypeID.hpp>

namespace legowl {

//! Peripheral device connected to a hub
class Peripheral {
public:
  explicit Peripheral(IOTypeID type) : _type{type} {}

  IOTypeID type() const { return _type; }

  virtual ~Peripheral() = default;

private:
  IOTypeID _type;
};

} // namespace legowl
