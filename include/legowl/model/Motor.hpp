#pragma once

#include <legowl/model/Peripheral.hpp>

#include <cassert>

namespace legowl {
//! Represents a rotational motor
class Motor : public Peripheral {
public:
  explicit Motor(IOTypeID type) noexcept : Peripheral(type) {}
};
} // namespace legowl
