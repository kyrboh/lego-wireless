#pragma once

#include <legowl/model/Peripheral.hpp>

#include <cstdint>
#include <memory>
#include <unordered_map>

namespace legowl {

class Hub {
public:
  explicit Hub() = default;

  Peripheral& device(std::uint8_t portId);

  void connect(std::uint8_t portId, std::unique_ptr<Peripheral> device);
  void disconnect(std::uint8_t portId);

private:
  std::unordered_map<std::uint8_t, std::unique_ptr<Peripheral>> _devices;
};

} // namespace legowl
