#pragma once

#include <cstdint>
#include <string_view>

namespace legowl {

//! Represents the type of hub alert
//!
//! https://lego.github.io/lego-ble-wireless-protocol-docs/index.html#alert-type
enum class HubAlertType : std::uint8_t {
  LowVoltage = 0x01,
  HighCurrent = 0x02,
  LowSignalStrength = 0x03,
  OverPowerCondition = 0x04,
};

std::string_view toString(HubAlertType alertType);

} // namespace legowl
