#pragma once

#include <cstdint>

namespace legowl {

//! Enumerates device numbers from advertisement manufacturer data
//!
//! FIXME: these must be used with different SystemTypes, need to enforce that
//!
//! https://lego.github.io/lego-ble-wireless-protocol-docs/#advertising
enum class DeviceNumber : std::uint8_t {
  WeDoHub = 0b00000,
  DuploTrain = 0b00000,
  BoostHub = 0b00000,
  TwoPortHub = 0b00001,
  TwoPortHandset = 0b00001,
  TechnicHub = 0b00000, // NOTE: obtained from a 4-port Technic Hub
};

} // namespace legowl
