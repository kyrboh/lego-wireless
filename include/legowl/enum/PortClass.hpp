#pragma once

#include <cstdint>

namespace legowl {
//! Designates port class by port id
//!
//! https://lego.github.io/lego-ble-wireless-protocol-docs/index.html#p-id
enum class PortClass : std::uint8_t {
  HubConnector, // 000 - 049
  Internal,     // 050 - 100
  Reserved,     // 101 - 255
};

PortClass toPortClass(std::uint8_t portId) noexcept;

} // namespace legowl
