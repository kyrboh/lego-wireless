#pragma once

#include <cstdint>

namespace legowl {

//! Enumerates system types in advertisement manufacturer data
//!
//! https://lego.github.io/lego-ble-wireless-protocol-docs/#system-type-and-device-number
enum class SystemType : std::uint8_t {
  LEGO_WEDO_2_0 = 0b000,
  LEGO_DUPLO = 0b001,
  LEGO_SYSTEM_010 = 0b010, // FIXME: documentation is not clear what ...
  LEGO_SYSTEM_011 = 0b011, // FIXME: ... these exactly are
  LEGO_TECHNIC = 0b100,    // NOTE: obtained from a 4-port Technic Hub
};

} // namespace legowl
