#pragma once

#include <cstdint>
#include <string_view>

namespace legowl {

//! Describes type of information obtained/requested from a port
//!
//! https://lego.github.io/lego-ble-wireless-protocol-docs/index.html#information-type
enum class PortInformationType : std::uint8_t {
  PortValue = 0x00,
  ModeInfo = 0x01,
  PossibleModeCombinations = 0x02,
};

std::string_view toString(PortInformationType infoType);

} // namespace legowl
