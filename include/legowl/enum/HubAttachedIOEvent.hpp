#pragma once

#include <legowl/util/CompilerQuirks.hpp>
#include <legowl/util/Constant.hpp>
#include <legowl/util/ErrorHandler.hpp>

#include <cstdint>
#include <string_view>

namespace legowl {

//! Event type for MessageType::HubAttachedIO
//!
//! \todo the name night be too generic
enum class HubAttachedIOEvent : std::uint8_t {
  DetachedIO = 0x00,
  AttachedIO = 0x01,
  AttachedVirtualIO = 0x02,
};

template <typename Callback>
auto route(HubAttachedIOEvent event, Callback callback) {
  switch (event) {
  case HubAttachedIOEvent::DetachedIO: return callback(constant<HubAttachedIOEvent::DetachedIO>{});
  case HubAttachedIOEvent::AttachedIO: return callback(constant<HubAttachedIOEvent::AttachedIO>{});
  case HubAttachedIOEvent::AttachedVirtualIO:
    return callback(constant<HubAttachedIOEvent::AttachedVirtualIO>{});
  }
  throwing::unknownEnumValue(event);
}

std::string_view toString(HubAttachedIOEvent event);

} // namespace legowl
