#pragma once

#include <cstdint>
#include <string_view>

namespace legowl {
//! Describes an operation on a hub alert
//!
//! TODO: denote direction (up/down)
//!
//! https://lego.github.io/lego-ble-wireless-protocol-docs/index.html#alert-operation
enum class HubAlertOperation : std::uint8_t {
  EnableUpdates = 0x01,
  DisableUpdates = 0x02,
  RequestUpdates = 0x03,
  Update = 0x04,
};

std::string_view toString(HubAlertOperation operation);
} // namespace legowl
