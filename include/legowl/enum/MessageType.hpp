#pragma once

#include <legowl/util/Constant.hpp>
#include <legowl/util/ErrorHandler.hpp>

#include <cstdint>
#include <iosfwd>
#include <string_view>
#include <type_traits>

namespace legowl {

//! Describes a message type attached to a CommonMessageHeader
//!
//! TODO: denote direction (up/down), responses, etc
//!
//! https://lego.github.io/lego-ble-wireless-protocol-docs/#message-typ
enum class MessageType : std::uint8_t {
  // Hub-related
  HubProperties = 0x01,
  HubActions = 0x02,
  HubAlerts = 0x03,
  HubAttachedIO = 0x04,
  GenericErrorMessages = 0x05,
  HWNetworkCommands = 0x08,
  FWUpdateGoIntoBootMode = 0x10,
  FWUpdateLockMemory = 0x11,
  FWUpdateLockStatusRequest = 0x12,
  FWLockStatus = 0x13,

  // Port-related
  PortInformationRequest = 0x21,
  PortModeInformationRequest = 0x22,
  PortInputFormatSetupSingle = 0x41,
  PortInputFormatSetupCombined = 0x42,
  PortInformation = 0x43,
  PortModeInformation = 0x44,
  PortValueSingle = 0x45,
  PortValueCombined = 0x46,
  PortInputFormatSingle = 0x47,
  PortInputFormatCombined = 0x48,
  VirtualPortSetup = 0x61,
  PortOutputCommand = 0x81,
  PortOutputCommandFeedback = 0x82,
};

//! Calls a function with an std::integer_constant<MessageType, mt>{}
//!
//! This allows to make mt a compile-time constant so it can be used
//! e.g. as a template argument
template <typename Callback>
constexpr auto route(MessageType mt, Callback callback) {
  switch (mt) {
  case MessageType::HubProperties: return callback(constant<MessageType::HubProperties>{});
  case MessageType::HubActions: return callback(constant<MessageType::HubActions>{});
  case MessageType::HubAlerts: return callback(constant<MessageType::HubAlerts>{});
  case MessageType::HubAttachedIO: return callback(constant<MessageType::HubAttachedIO>{});
  case MessageType::GenericErrorMessages:
    return callback(constant<MessageType::GenericErrorMessages>{});
  case MessageType::HWNetworkCommands: return callback(constant<MessageType::HWNetworkCommands>{});
  case MessageType::FWUpdateGoIntoBootMode:
    return callback(constant<MessageType::FWUpdateGoIntoBootMode>{});
  case MessageType::FWUpdateLockMemory:
    return callback(constant<MessageType::FWUpdateLockMemory>{});
  case MessageType::FWUpdateLockStatusRequest:
    return callback(constant<MessageType::FWUpdateLockStatusRequest>{});
  case MessageType::FWLockStatus: return callback(constant<MessageType::FWLockStatus>{});
  case MessageType::PortInformationRequest:
    return callback(constant<MessageType::PortInformationRequest>{});
  case MessageType::PortModeInformationRequest:
    return callback(constant<MessageType::PortModeInformationRequest>{});
  case MessageType::PortInputFormatSetupSingle:
    return callback(constant<MessageType::PortInputFormatSetupSingle>{});
  case MessageType::PortInputFormatSetupCombined:
    return callback(constant<MessageType::PortInputFormatSetupCombined>{});
  case MessageType::PortInformation: return callback(constant<MessageType::PortInformation>{});
  case MessageType::PortModeInformation:
    return callback(constant<MessageType::PortModeInformation>{});
  case MessageType::PortValueSingle: return callback(constant<MessageType::PortValueSingle>{});
  case MessageType::PortValueCombined: return callback(constant<MessageType::PortValueCombined>{});
  case MessageType::PortInputFormatSingle:
    return callback(constant<MessageType::PortInputFormatSingle>{});
  case MessageType::PortInputFormatCombined:
    return callback(constant<MessageType::PortInputFormatCombined>{});
  case MessageType::VirtualPortSetup: return callback(constant<MessageType::VirtualPortSetup>{});
  case MessageType::PortOutputCommand: return callback(constant<MessageType::PortOutputCommand>{});
  case MessageType::PortOutputCommandFeedback:
    return callback(constant<MessageType::PortOutputCommandFeedback>{});
  }

  // TODO: make router more sophisticated (idea: store/return optional)
  throwing::unknownEnumValue(mt);
}

std::string_view toString(MessageType mt);

std::ostream &operator<<(std::ostream &os, MessageType messageType);

} // namespace legowl
