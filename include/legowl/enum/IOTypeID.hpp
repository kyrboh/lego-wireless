#pragma once

#include <cstdint>
#include <string_view>

namespace legowl {

//! Type of the IO
//!
//! https://lego.github.io/lego-ble-wireless-protocol-docs/index.html#io-typ
enum class IOTypeID : std::uint16_t {
  Motor = 0x0001,
  SystemTrainMotor = 0x0002,
  Button = 0x0005,
  LEDLight = 0x0008,
  Voltage = 0x0014,
  Current = 0x0015,
  PiezoToneSound = 0x0016,
  RGBLight = 0x0017,
  ExternalTiltSensor = 0x0022,
  MotionSensor = 0x0023,
  VisionSensor = 0x0025,
  ExternalMotorWithTacho = 0x0026,
  InternalMotorWithTacho = 0x0027,
  InternalTilt = 0x0028,

  // Deduced experimentally
  PoweredUpXLMotor = 0x2f00,
  PoweredUpLMotor = 0x2e00,
};

std::string_view toString(IOTypeID ioType);

} // namespace legowl
