#pragma once

#include <cstdint>
#include <string_view>

namespace legowl {

enum class ErrorCode : std::uint8_t {
  ACK = 0x01,  // TODO: what does this mean?
  MACK = 0x02, // TODO: what does this mean?
  BufferOverflow = 0x03,
  Timeout = 0x04,
  CommandNotRecognized = 0x05,
  InvalidUse = 0x06,  // e.g. parameter error(s)
  Overcurrent = 0x07, // sent with the command that is not going to be executed
  InternalError = 0x08,
};

std::string_view toString(ErrorCode ec);

} // namespace legowl
