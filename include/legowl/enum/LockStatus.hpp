#pragma once

#include <cstdint>
#include <string_view>

namespace legowl {

//! Firmware lock status
//!
//! https://lego.github.io/lego-ble-wireless-protocol-docs/index.html#f-w-lock-status
enum LockStatus : std::uint8_t {
  Ok = 0x00,
  NotLocked = 0xFF,
};

std::string_view toString(LockStatus status);

} // namespace legowl
