#pragma once

#include <legowl/util/Span.hpp>

#include <cstdint>
#include <string>

namespace legowl {
//! Represents a version number for S/W or H/W
//!
//! https://lego.github.io/lego-ble-wireless-protocol-docs/index.html#ver-no
class Version {
public:
  explicit Version(std::uint8_t major,
                   std::uint8_t minor,
                   std::uint8_t patch,
                   std::uint16_t build) :
      _major{major},
      _minor{minor},
      _patch{patch},
      _build{build} {}

  static Version read(Span<const std::uint8_t> bytes);

  std::uint8_t major() const { return _major; }
  std::uint8_t minor() const { return _minor; }
  std::uint8_t patch() const { return _patch; }
  std::uint16_t build() const { return _build; }

  //! "Official" string representation
  std::string asString() const;

  //! String representation w/ the build number
  std::string asFullString() const;

private:
  std::uint8_t _major : 4;
  std::uint8_t _minor : 4;
  std::uint8_t _patch;
  std::uint16_t _build;
};

static_assert(sizeof(Version) == sizeof(std::int32_t));

} // namespace legowl
