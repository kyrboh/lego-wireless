#pragma once

#include <legowl/enum/DeviceNumber.hpp>
#include <legowl/enum/SystemType.hpp>

#include <cstdint>

namespace legowl {

//! Represents a pair of SystemType and DeviceNumber
class DeviceInfo {
public:
  constexpr explicit DeviceInfo(std::uint8_t data) :
      _systemType{static_cast<SystemType>((data & 0b11100000) >> 5)},
      _deviceNo{static_cast<DeviceNumber>(data & 0b00011111)} {}

  constexpr SystemType systemType() const { return _systemType; }
  constexpr DeviceNumber deviceNumber() const { return _deviceNo; }

private:
  SystemType _systemType : 3;
  DeviceNumber _deviceNo : 5;
};

} // namespace legowl
